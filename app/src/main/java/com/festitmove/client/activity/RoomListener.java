package com.festitmove.client.activity;

import java.util.List;

public interface RoomListener {

    public void getRooms(List<String> rooms);
}
