package com.festitmove.client.activity;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.festitmove.client.controller.AuthentificationController;
import com.festitmove.client.controller.impl.AuthentificationControllerImpl;

import okhttp3.OkHttpClient;


public class MainApplication extends Application {

    private AuthentificationController controler;

    @Override
    public void onCreate() {
        super.onCreate();
        controler = new AuthentificationControllerImpl(new OkHttpClient());
    }

    public AuthentificationController getControler() {
        return controler;
    }
}
