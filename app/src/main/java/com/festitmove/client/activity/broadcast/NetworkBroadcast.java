package com.festitmove.client.activity.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkBroadcast extends BroadcastReceiver {

    public static final String TAG = NetworkBroadcast.class.getSimpleName();
    private final NetworkAvailability networkAvailability;

    public NetworkBroadcast(NetworkAvailability networkAvailability){
        this.networkAvailability = networkAvailability;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager
                    .getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                Log.i(TAG, "network available");
                networkAvailability.onNetworkAvailable();
            } else {
                Log.i(TAG, "network available not");
                networkAvailability.onNetworkUnavailable();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
