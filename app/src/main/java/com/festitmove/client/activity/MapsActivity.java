package com.festitmove.client.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.wicked.myapplication.R;
import com.festitmove.client.activity.broadcast.NetworkAvailabilityImpl;
import com.festitmove.client.activity.broadcast.NetworkBroadcast;
import com.festitmove.client.controller.impl.MainControllerImpl;
import com.festitmove.client.net.WebSocketClient;
import com.festitmove.client.net.WebSocketClientImpl;
import com.festitmove.client.repository.MarkerRepository;
import com.festitmove.client.repository.impl.MarkerRepositoryImpl;
import com.festitmove.client.controller.MainController;
import com.festitmove.client.activity.util.PermissionUtils;
import com.festitmove.client.repository.UserRepository;
import com.festitmove.client.repository.impl.UserRepositoryImpl;
import com.festitmove.client.service.NetworkService;
import com.festitmove.client.service.impl.MarkerService;
import com.festitmove.client.service.impl.NetworkServiceImpl;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MapStyleOptions;

import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static android.net.ConnectivityManager.CONNECTIVITY_ACTION;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String TAG = MapsActivity.class.getSimpleName();

    final static private String[] appPermissions = {
            android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.INTERNET
    };
    private static final int PERMISSION_FOR_LAST_KNOWN_LOCATION = 10;
    private static final int PERMISSION_FOR_REGISTRATION = 20;
    private static final long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private static final long FASTEST_INTERVAL = 2000; /* 2 sec */
    private String username;
    private MainController mainController;
    private LocationRequest locationRequest;
    private RoomDrawer roomDrawer;
    private String token;
    private NetworkAvailabilityImpl networkAvailability;
    private NetworkBroadcast networkBroadcast;
    private IntentFilter intentFilterForNetwork;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "Starting MapsActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        roomDrawer = new RoomDrawer(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        networkAvailability = new NetworkAvailabilityImpl();
        username = getIntent().getExtras().getString("username");
        token = getIntent().getExtras().getString("token");
        intentFilterForNetwork = new IntentFilter(CONNECTIVITY_ACTION);
    }

    /*
        Inregistram broadcast-ul de internet la pornirea aplicației sau la revenirea
        din alte activități
     */
    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "onStart registering broadcast");
        networkBroadcast = new NetworkBroadcast(networkAvailability);
        registerReceiver(networkBroadcast, intentFilterForNetwork);
    }

    /*
         Unregister network broadcast for when the activity is not on foreground.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy, unregister network broadcast");
        unregisterReceiver(networkBroadcast);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        applyCustomMapStyle(googleMap);

        // Using pure dependency injection method
        MarkerRepository markerRepository = new MarkerRepositoryImpl(googleMap);
        UserRepository userRepository = new UserRepositoryImpl();
        MarkerService markerService = new MarkerService(username, markerRepository, userRepository);
        WebSocketClient webSocketClient = new WebSocketClientImpl();
        NetworkService networkServiceImpl = new NetworkServiceImpl(webSocketClient, markerService, roomDrawer);
        mainController = new MainControllerImpl(markerService, networkServiceImpl);
        roomDrawer.setController(mainController);
        networkAvailability.setMainController(mainController);
        networkAvailability.setToken(token);

        // On other thread
        Log.i(TAG, "map-ready: " + googleMap + " " + mainController + " " + networkServiceImpl);
        mainController.startService(token);
        startLocationUpdates();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_FOR_REGISTRATION:
                for (int perm : grantResults) {
                    if (PERMISSION_DENIED == perm) {
                        logout();
                    }
                }
                registerLocationUpdates();
                break;
            case PERMISSION_FOR_LAST_KNOWN_LOCATION:
                for (int perm : grantResults) {
                    if (PERMISSION_DENIED == perm) {
                        logout();
                    }
                }
                getLastLocation();
                break;
            default:
                break;
        }
    }

    private void logout() {
        if(mainController!=null){
            mainController.stopService();
        }
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void startLocationUpdates() {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        registerLocationUpdates();
    }

    private void registerLocationUpdates() {
        if (!userNeedsPermissions()) {
            try {
                getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, new LocationCallback() {
                            @Override
                            public void onLocationResult(LocationResult locationResult) {
                                onLocationChanged(locationResult.getLastLocation());
                            }
                        },
                        Looper.myLooper());
            } catch (SecurityException se) {
                Log.e(TAG, se + " permissions not granted");
            }
        } else {
            requestPermissions(appPermissions, PERMISSION_FOR_REGISTRATION);
        }
    }

    public void getLastLocation() {
        if (!userNeedsPermissions()) {
            try {
                getFusedLocationProviderClient(this)
                        .getLastLocation()
                        .addOnSuccessListener(location -> {
                            if (location != null) {
                                onLocationChanged(location);
                            }
                        })
                        .addOnFailureListener(e -> {
                            Log.d(TAG, "Error trying to get last GPS location");
                            e.printStackTrace();
                        });
            } catch (SecurityException se) {
                Log.e(TAG, se + " permissions not granted");
            }
        } else {
            requestPermissions(appPermissions, PERMISSION_FOR_REGISTRATION);
        }
    }


    private void onLocationChanged(Location lastLocation) {
        Log.i(TAG, lastLocation.getLatitude() + " lat and long: " + lastLocation.getLongitude());
        if (mainController != null) {
            mainController.updateUserMarker(username, lastLocation.getLatitude(), lastLocation.getLongitude());
        }
    }

    /**
     * @return true if user needs runtime permissions, false if he already enabled them.
     */
    private boolean userNeedsPermissions() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && PermissionUtils.doesNotHavePermissions(this, appPermissions);
    }

    private void applyCustomMapStyle(GoogleMap googleMap) {
        try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.style_json));
            if (!success) {
                Log.i(TAG, "Style parsing failed.");
            } else {
                Log.i(TAG, "DONE RIGHT!");
            }
        } catch (Resources.NotFoundException e) {
            Log.i(TAG, "Can't find style. Error: ", e);
        }
    }
}
