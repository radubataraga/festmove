package com.festitmove.client.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.wicked.myapplication.R;
import com.festitmove.client.controller.MainController;

import java.util.List;

import static android.content.ContentValues.TAG;

class RoomDrawer extends DrawerLayout.SimpleDrawerListener implements NavigationView.OnNavigationItemSelectedListener, RoomListener {

    private final MapsActivity mapsActivity;
    private final Menu navigationMenu;
    private MainController mainControllerImpl;

    RoomDrawer(MapsActivity applicationContext) {
        this.mapsActivity = applicationContext;
        Toolbar toolbar = applicationContext.findViewById(R.id.toolbar);
        DrawerLayout drawer = applicationContext.findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                mapsActivity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(this);
        toggle.syncState();
        NavigationView navigationView = mapsActivity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationMenu = navigationView.getMenu();
    }

    void setController(MainController mainControllerImpl) {
        this.mainControllerImpl = mainControllerImpl;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.logout_menu){
            logout();
            return true;
        }
        Log.i(TAG, "pressed on room " + item.getTitle().toString());
        mainControllerImpl.changeRoom(item.getTitle().toString());
        mapsActivity.getLastLocation();
        DrawerLayout drawer = mapsActivity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        mainControllerImpl.refreshRooms();
    }

    @Override
    public void getRooms(List<String> rooms) {
        mapsActivity.runOnUiThread(() -> {
            if (rooms != null) {
                navigationMenu.removeGroup(R.id.my_group);
                for (String room : rooms) {
                    MenuItem menuItem = navigationMenu.add(R.id.my_group, Menu.NONE, Menu.FIRST, room);
                    menuItem.setIcon(R.drawable.ic_menu_slideshow);
                    menuItem.setCheckable(true);
                }
            }
        });
    }

    private void logout() {
        mainControllerImpl.stopService();
        Intent intent = new Intent(mapsActivity.getApplicationContext(),LoginActivity.class);
        mapsActivity.startActivity(intent);
        mapsActivity.finish();
    }
}
