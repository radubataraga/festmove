package com.festitmove.client.activity.broadcast;

import com.festitmove.client.controller.MainController;

public class NetworkAvailabilityImpl implements NetworkAvailability {

    private MainController mainControllerImpl;
    private String token;

    public void setMainController(MainController mainControllerImpl) {
        this.mainControllerImpl = mainControllerImpl;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public void onNetworkAvailable() {
        if (mainControllerImpl != null && token != null) {
            mainControllerImpl.startService(token);
            mainControllerImpl.refreshRooms();
        }
    }

    @Override
    public void onNetworkUnavailable() {
        if (mainControllerImpl != null) {
            mainControllerImpl.stopService();
        }
    }
}
