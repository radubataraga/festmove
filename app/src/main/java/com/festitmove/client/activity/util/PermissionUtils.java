package com.festitmove.client.activity.util;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class PermissionUtils {

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean doesNotHavePermissions(Context context, String... permissions) {
        return !hasPermissions(context,permissions);
    }

    public static void logThread(String TAG) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            Log.i(TAG, "It's the UI thread:" + Thread.currentThread().getName());
        } else {
            Log.i(TAG, "It's not the UI thread" +Thread.currentThread().getName());
        }
    }
}
