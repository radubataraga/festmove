package com.festitmove.client.activity.broadcast;

interface NetworkAvailability {
    void onNetworkAvailable();
    void onNetworkUnavailable();
}
