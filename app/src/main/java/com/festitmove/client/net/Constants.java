package com.festitmove.client.net;

public class Constants {
    static final String AUTHORIZE_SERVER_PORT = "8090";
    public static final String RESOURCE_SERVER_PORT = "8090";
    static final String AUTHORIZE_SERVER_IP = "192.168.16.101";
    public static final String RESOURCE_SERVER_IP = "192.168.16.101";

    public static final String WEBSOCKET_ENDPOINT = "/uaa/ws-markers/websocket";
    static final String HEADER_AUTHORIZATION = "Authorization";
    static final String AUTHORIZATION_BEARER_TYPE = "Bearer ";
    static final String USERNAME = "username";
    static final String PASSWORD = "password";
    static final String GRANT_TYPE = "grant_type";
    static final String CLIENT_ID = "client_id";
    static final String CLIENT_GRANT_TYPE = "password";
    static final String CLIENT_APPLICATION_ID = "clientIdPassword";
    static final String CLIENT_APPLICATION_SECRET = "secret";

    private Constants() {
        //Intentionally left blank
    }
}
