package com.festitmove.client.net;

import android.os.AsyncTask;
import android.util.Log;

import com.festitmove.client.model.security.Token;
import com.festitmove.client.net.listener.OnErrorListener;
import com.festitmove.client.net.listener.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.festitmove.client.net.Constants.AUTHORIZE_SERVER_IP;
import static com.festitmove.client.net.Constants.AUTHORIZE_SERVER_PORT;
import static com.festitmove.client.net.Constants.CLIENT_APPLICATION_ID;
import static com.festitmove.client.net.Constants.CLIENT_APPLICATION_SECRET;
import static com.festitmove.client.net.Constants.CLIENT_GRANT_TYPE;
import static com.festitmove.client.net.Constants.CLIENT_ID;
import static com.festitmove.client.net.Constants.GRANT_TYPE;
import static com.festitmove.client.net.Constants.PASSWORD;
import static com.festitmove.client.net.Constants.USERNAME;

public final class LoginTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = LoginTask.class.getSimpleName();
    private final String username;
    private final String password;
    private final OkHttpClient okHttpClient;
    private OnSuccessListener<String> onSuccessListener;
    private OnErrorListener onErrorListener;

    public LoginTask(String username, String password, OnSuccessListener<String> onSuccessListener, OnErrorListener
            onErrorListenerm, OkHttpClient okHttpClient) {
        this.onSuccessListener = onSuccessListener;
        this.onErrorListener = onErrorListenerm;
        this.okHttpClient = okHttpClient;
        this.username = username;
        this.password = password;
    }

    @Override
    protected String doInBackground(Void[] params) {
        String credential = Credentials.basic(CLIENT_APPLICATION_ID, CLIENT_APPLICATION_SECRET);
        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        formBodyBuilder.add(GRANT_TYPE, CLIENT_GRANT_TYPE);
        formBodyBuilder.add(CLIENT_ID, CLIENT_APPLICATION_ID);
        formBodyBuilder.add(USERNAME, username);
        formBodyBuilder.add(PASSWORD, password);
        Request request = createRequest(formBodyBuilder, credential);

        try {
            processResult(request);
        } catch (Exception e) {
            Log.i(TAG, "failed registration + in catch" + e.getLocalizedMessage());
            onErrorListener.onFailure(e.getMessage());
            return "failure";
        }
        Log.d(TAG, "finished logging");
        return "success";
    }

    private void processResult(Request request) throws IOException {
        Response response = okHttpClient.newBuilder()
                .build().newCall(request).execute();

        if (response.isSuccessful() && response.body() != null) {
            Log.i(TAG, "successfull login");
            Gson mGson = new GsonBuilder().create();
            Token token = mGson.fromJson(response.body().string(), Token.class);
            onSuccessListener.onSuccess(token.getAccess_token());
        } else {
            Log.i(TAG, "failed login" + response.toString());
            onErrorListener.onFailure(response.toString());
        }
    }

    private Request createRequest(FormBody.Builder formBodyBuilder, String credential) {
        return new Request.Builder()
                .header(Constants.HEADER_AUTHORIZATION, credential)
                .url("http://" + AUTHORIZE_SERVER_IP + ":" + AUTHORIZE_SERVER_PORT + "/uaa/oauth/token")
                .post(formBodyBuilder.build())
                .build();
    }
}
