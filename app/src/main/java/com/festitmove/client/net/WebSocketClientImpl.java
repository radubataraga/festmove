package com.festitmove.client.net;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import io.reactivex.FlowableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.WebSocket;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.StompHeader;
import ua.naiksoftware.stomp.client.StompClient;
import ua.naiksoftware.stomp.client.StompMessage;

import static com.festitmove.client.net.Constants.AUTHORIZATION_BEARER_TYPE;
import static com.festitmove.client.net.Constants.HEADER_AUTHORIZATION;


public class WebSocketClientImpl implements WebSocketClient {

    private static final String TAG = WebSocketClientImpl.class.getSimpleName();

    private StompClient mStompClient;
    private List<Disposable> subscriptionList = new ArrayList<>();
    private AtomicBoolean isConnected = new AtomicBoolean(false);
    private ScheduledFuture<?> heartbeat;


    public WebSocketClientImpl() {
    }

    @Override
    public void connectStomp(String ip, String port, String endpoint, String token) {

        Map<String, String> headers = new HashMap<>();
        headers.put(HEADER_AUTHORIZATION, AUTHORIZATION_BEARER_TYPE + token);
        mStompClient = Stomp.over(WebSocket.class, "ws://" + ip + ":" + port + endpoint, headers);
        mStompClient.lifecycle()
                .subscribeOn(Schedulers.single())
                .observeOn(Schedulers.single())
                .subscribe(lifecycleEvent -> {
                    switch (lifecycleEvent.getType()) {
                        case OPENED:
                            isConnected.set(true);
                            Log.i(TAG, "Stomp connection opened", lifecycleEvent.getException());
                            startHeartbeat();
                            break;
                        case ERROR:
                            disconnectStomp();
                            stopHeartbeat();
                            isConnected.set(false);
                            Log.e(TAG, "Stomp connection error", lifecycleEvent.getException());
                            break;
                        case CLOSED:
                            isConnected.set(false);
                            stopHeartbeat();
                            disconnectStomp();
                            Log.e(TAG, "Stomp connection closed", lifecycleEvent.getException());
                    }
                });
        mStompClient.connect(false);
    }

    @Override
    public void sendEchoViaStomp(String url, String data) {
        if (isConnected.get()) {
            Log.i(TAG, "Doing request for endpoint " + url + " with data: " + data);
            mStompClient.send("/app" + url, data)
                    .compose(applySchedulers())
                    .subscribe(aVoid -> {
                        Log.i(TAG, "STOMP echo send successfully");
                    }, throwable -> {
                        Log.i(TAG, "Error send STOMP echo", throwable);
                    });
        }
    }

    @Override
    public void sendEchoViaStomp(String url) {
        if (isConnected.get()) {
            Log.i(TAG, "Doing request for endpoint " + url);
            mStompClient.send("/app" + url)
                    .compose(applySchedulers())
                    .subscribe(aVoid -> {
                        Log.i(TAG, "STOMP echo send successfully");
                    }, throwable -> {
                        Log.i(TAG, "Error send STOMP echo", throwable);
                    });
        }
    }

    @Override
    public void subscribe(String path, SocketListener<String> socketListener) {
        if (isConnected.get()) {
            Disposable subscription = mStompClient.topic(path)
                    .subscribeOn(Schedulers.single())
                    .observeOn(Schedulers.single())
                    .subscribe(topicMessage -> {
                        Log.i(TAG, "Received " + topicMessage.getPayload().trim());
                        socketListener.onUpdate(topicMessage.getPayload().trim());
                    });
            subscriptionList.add(subscription);
        }
    }

    @Override
    public void unsubscribeAll() {
        if (isConnected.get()) {
            if (subscriptionList != null && subscriptionList.size() != 0) {
                for (Disposable subscription : subscriptionList) {
                    Log.i(TAG, "removing sub: " + subscription.toString());
                    subscription.dispose();
                }
                subscriptionList.clear();
            }
        }
    }

    @Override
    public boolean isConnected() {
        return isConnected.get();
    }

    @Override
    public boolean isConnecting() {
        return mStompClient != null && mStompClient.isConnecting();
    }

    @Override
    public void disconnectStomp() {
        isConnected.set(false);
        unsubscribeAll();
        //TODO: remove this disconnect frame
        mStompClient.send(new StompMessage("DISCONNECT", new ArrayList<>(), "DISCONNECT"));
        mStompClient.disconnect();
    }

    //TODO: Do websocket connection and serving in background threads.
    private <T> FlowableTransformer<T, T> applySchedulers() {
        return tFlowable -> tFlowable
                .unsubscribeOn(Schedulers.newThread())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void startHeartbeat() {
        heartbeat = Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
            Log.e(TAG, "SEND HEARTBEAT " + "FROM SERVICE");
            if (null != mStompClient) {
                mStompClient.send("", "h").subscribe();
            }
        }, 0, 45, TimeUnit.SECONDS);
    }

    private void stopHeartbeat() {
        if (null != heartbeat) {
            heartbeat.cancel(true);
            heartbeat = null;
        }
    }
}
