package com.festitmove.client.net.listener;

public interface OnErrorListener {
    void onFailure(String failure);
}
