package com.festitmove.client.net.listener;

public interface OnSuccessListener<T> {
    void onSuccess(T value);
}
