package com.festitmove.client.net;

import android.os.AsyncTask;
import android.util.Log;

import com.festitmove.client.net.listener.OnErrorListener;
import com.festitmove.client.net.listener.OnSuccessListener;
import java.io.IOException;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import static com.festitmove.client.net.Constants.AUTHORIZE_SERVER_IP;
import static com.festitmove.client.net.Constants.AUTHORIZE_SERVER_PORT;
import static com.festitmove.client.net.Constants.PASSWORD;
import static com.festitmove.client.net.Constants.USERNAME;

public class RegisterTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = RegisterTask.class.getSimpleName();

    private static final String SUCCES = "succes";
    private final OnErrorListener onFailureListener;
    private final OnSuccessListener<String> onSuccesListener;
    private final String password;
    private final String username;
    private final OkHttpClient okHttpClient;

    public RegisterTask(String username, String password, OnSuccessListener<String> onSuccessListener, OnErrorListener onErrorListener, OkHttpClient okHttpClient) {
        this.username = username;
        this.password = password;
        this.onSuccesListener = onSuccessListener;
        this.onFailureListener = onErrorListener;
        this.okHttpClient = okHttpClient;
    }

    @Override
    protected String doInBackground(Void... params) {

        if (username == null || password == null) {
            onFailureListener.onFailure("failure");
            return "failure";
        }

        FormBody.Builder formBodyBuilder = new FormBody.Builder();
        formBodyBuilder.add(USERNAME, username);
        formBodyBuilder.add(PASSWORD, password);
        Request request = createRequest(formBodyBuilder);

        try {
            processResult(request);
        } catch (Exception e) {
            Log.i(TAG, "failed registration + in catch" + e.getLocalizedMessage());
            onFailureListener.onFailure(e.getMessage());
        }
        return SUCCES;
    }

    private void processResult(Request request) throws IOException {

        Response response = okHttpClient.newBuilder()
                .build().newCall(request).execute();
        if (response.isSuccessful()){
            Log.i(TAG, "successfully registration");
            onSuccesListener.onSuccess(response.toString());
        }else{
            Log.i(TAG, "failed registration" + response.toString());
            onFailureListener.onFailure(response.toString());
        }
    }

    private Request createRequest(FormBody.Builder formBodyBuilder) {
        return new Request.Builder()
                    .url("http://" + AUTHORIZE_SERVER_IP + ":" + AUTHORIZE_SERVER_PORT + "/uaa/register")
                    .post(formBodyBuilder.build())
                    .build();
    }
}
