package com.festitmove.client.net;

public interface SocketListener<T> {
    public void onUpdate(T result);
}
