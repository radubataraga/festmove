package com.festitmove.client.net;

public interface WebSocketClient {

    /**
     * Used for creating a STOMP connection over websocket using
     * the received parameters.
     * If a connection a connection is already set-up then
     * a reconnection won't be made.
     * @param ip the ip of the server
     * @param port the port of the server
     * @param endpoint the endpoint of the server
     * @param token the token needed by server for authorization
     */
    void connectStomp(String ip, String port, String endpoint, String token);

    /**
     * Send message at the given url
     * @param url the destination message
     * @param data the message
     */
    void sendEchoViaStomp(String url, String data);

    /**
     * send an empty message to the given url
     * @param url the destination message
     */
    void sendEchoViaStomp(String url);

    /**
     * Create a subscription by giving a {@link SocketListener}
     * for a destination.
     * @param path the destion
     * @param socketListener the socket listener
     */
    void subscribe(String path, SocketListener<String> socketListener);

    /**
     * Unsubscribe to all created subscriptions.
     */
    void unsubscribeAll();

    /**
     * @return verifies if a socket connection is made and it's still good.
     */
    boolean isConnected();

    /**
     * @return verifies if a socket connection is being made.
     */
    boolean isConnecting();

    /**
     * Disconnect the stomp connection over socket.
     */
    void disconnectStomp();
}
