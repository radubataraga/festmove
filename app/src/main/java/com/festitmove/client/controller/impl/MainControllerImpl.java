package com.festitmove.client.controller.impl;

import android.util.Log;

import com.festitmove.client.controller.MainController;
import com.festitmove.client.service.CurrentUserMarkerHandler;
import com.festitmove.client.service.NetworkService;
import com.google.android.gms.maps.model.LatLng;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.festitmove.client.controller.util.JsonParserUtil.prepareJsonString;


public class MainControllerImpl implements MainController {

    private static final String TAG = MainController.class.getSimpleName();
    private static final ExecutorService es = Executors.newCachedThreadPool();
    private final NetworkService networkServiceImpl;
    private final CurrentUserMarkerHandler currentUserMarkerHandler;

    public MainControllerImpl(CurrentUserMarkerHandler currentUserMarkerHandler, NetworkService networkServiceImpl) {
        this.networkServiceImpl = networkServiceImpl;
        this.currentUserMarkerHandler = currentUserMarkerHandler;
    }

    @Override public void startService(String token) {
        es.execute(() -> networkServiceImpl.connect(token));
    }

    @Override public void stopService() {
        es.execute(networkServiceImpl::disconnect);
    }

    @Override public void updateUserMarker(String currentUsername, double latitude, double longitude) {
        es.execute(() -> {
            Log.i(TAG, "In submit for updateUserMarker");
            LatLng latLng = new LatLng(latitude, longitude);
            currentUserMarkerHandler.refreshUserMarker(latLng);
            String message = prepareJsonString(currentUsername, latLng);
            networkServiceImpl.refreshUserMarker(message);
            Log.i(TAG, " am incercat sa updatez pozitia clientului");
        });
    }

    @Override public void changeRoom(String room) {
        es.execute(() ->{
           Log .i(TAG,"In submit for changeRoom, in room " + room );
            networkServiceImpl.changeRoom(room);
            networkServiceImpl.synchronizeMarkers();
        });
    }

    @Override public void refreshRooms() {
        es.execute(networkServiceImpl::refreshRooms);
    }
}
