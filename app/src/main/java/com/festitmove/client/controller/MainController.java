package com.festitmove.client.controller;

public interface MainController {

    /**
     * This starts the network service.
     * @param token the token used for connection
     */
    void startService(String token);

    /**
     * Stops the network service
     */
    void stopService();

    /**
     * Updates the current user marker on the google map and by sending
     * the new coordonates to the server
     * @param currentUsername the user's username
     * @param latitude the user's latitude
     * @param longitude the user's longitude
     */
    void updateUserMarker(String currentUsername, double latitude, double longitude);

    /**
     * Requests network service to change the current room to {@param room}
     * @param room the room user want's to be changed
     */
    void changeRoom(String room);

    /**
     * User requests that current room should be refreshed.
     */
    void refreshRooms();
}
