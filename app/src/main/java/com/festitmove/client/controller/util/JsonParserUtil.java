package com.festitmove.client.controller.util;

import com.google.android.gms.maps.model.LatLng;

import javax.json.Json;

public class JsonParserUtil {
    public static String prepareJsonString(String currentUsername, LatLng latLng) {
        return Json.createObjectBuilder()
                .add("name", currentUsername)
                .add("longitude", latLng.longitude)
                .add("latitude", latLng.latitude)
                .build()
                .toString();
    }
}
