package com.festitmove.client.controller.impl;

import android.util.Log;

import com.festitmove.client.net.LoginTask;
import com.festitmove.client.controller.AuthentificationController;
import com.festitmove.client.net.RegisterTask;
import com.festitmove.client.net.listener.OnErrorListener;
import com.festitmove.client.net.listener.OnSuccessListener;

import java.util.concurrent.locks.ReentrantLock;

import okhttp3.OkHttpClient;


public class AuthentificationControllerImpl implements AuthentificationController {

    private static final String TAG = AuthentificationControllerImpl.class.getSimpleName();
    private final OkHttpClient okHttpClient;
    private LoginTask loginTask;
    private RegisterTask registerTask;
    private ReentrantLock loginLock = new ReentrantLock();

    public AuthentificationControllerImpl(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public void login(String username, String password,
                      final OnSuccessListener<String> onSuccessListener,
                      final OnErrorListener onErrorListener) {

        Log.d(TAG, "Trying to login..");
        if (loginLock.tryLock()) {
            if (loginTask != null) {
                loginTask.cancel(true);
            }
            loginTask = new LoginTask(username, password, onSuccessListener, onErrorListener, okHttpClient);
            loginLock.unlock();
        }
        loginTask.execute();
    }

    @Override
    public void register(String username, String password, OnSuccessListener<String> onSuccessListener, OnErrorListener onErrorListener) {
        Log.d(TAG, "Trying to register..");
        if (loginLock.tryLock()) {
            if (registerTask != null) {
                registerTask.cancel(true);
            }
            registerTask = new RegisterTask(username, password, onSuccessListener, onErrorListener, okHttpClient);
            loginLock.unlock();
        }
        registerTask.execute();
    }
}
