package com.festitmove.client.service;

public interface NetworkService {
    /**
     * Creates a websocket connexion with the server.
     *
     * @param token that is required for the connexion with the server.
     */
    void connect(String token);

    /**
     *  Switches the actual room with the inputed room.
     *  All subscriptions of the previous room is lost.
     *
     * @param room the new room that user is connecting
     */
    void changeRoom(String room);

    /**
     *  Request the server to send the availables rooms.
     *
     */
    void refreshRooms();

    /**
     * User sends the updated marker position along with his username.
     *
     * @param marker the user marker that is sent to the server.
     */
    void refreshUserMarker(String marker);

    /**
     * Requests the server to send all the markers for the current room.
     */
    void synchronizeMarkers();

    /**
     * Disconnects from the current websocket.
     */
    void disconnect();
}
