package com.festitmove.client.service.impl;

import android.util.Log;

import com.festitmove.client.activity.RoomListener;
import com.festitmove.client.model.MarkerResponse;
import com.festitmove.client.net.WebSocketClient;
import com.festitmove.client.service.NetworkService;
import com.festitmove.client.service.listener.MarkerListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import static com.festitmove.client.net.Constants.RESOURCE_SERVER_IP;
import static com.festitmove.client.net.Constants.RESOURCE_SERVER_PORT;
import static com.festitmove.client.net.Constants.WEBSOCKET_ENDPOINT;

public class NetworkServiceImpl implements NetworkService {

    private static final String TAG = NetworkServiceImpl.class.getName();
    private static final String SUBSCRIBE_URL_UPDATE_MARK = "/topic/marker/update";
    private static final String SUBSCRIBE_URL_FOR_USER_CHANGING_ROOM = "/topic/marker/remove";
    private static final String SUBSCRIBE_URL_REFRESH_ROOMS = "/user/queue/rooms/getall";
    private static final String SUBSCRIBE_URL_SYNCRONIZE = "/user/queue/marker/getall";
    private static final String REQUEST_URL_UPDATE_MARKER = "/marker/update";
    private static final String NOTIFY_URL_FOR_ROOM_CHANGED = "/room/update";
    private static final String REQUEST_URL_REFRESH_ROOMS = "/room/getall";
    private static final String REQUEST_URL_SYNCRONZE_MARKER = "/marker/getall";
    private static final String SLASH = "/";
    private String currentRoom = "room2";

    private final Type listOfMarkerResponseType = new TypeToken<List<MarkerResponse>>() {}.getType();
    private final Type listOfRooms = new TypeToken<List<String>>() {}.getType();
    private final WebSocketClient webSocketClient;
    private final MarkerListener markerListener;
    private final Gson mGson = new GsonBuilder().create();
    private final RoomListener roomsListener;

    public NetworkServiceImpl(WebSocketClient webSocketClient, MarkerListener markerListener, RoomListener roomsListener) {
        this.webSocketClient = webSocketClient;
        this.markerListener = markerListener;
        this.roomsListener = roomsListener;
    }

    @Override
    public synchronized void connect(String token) {
        if (!webSocketClient.isConnected() && !webSocketClient.isConnecting()) {
            webSocketClient.connectStomp(RESOURCE_SERVER_IP, RESOURCE_SERVER_PORT, WEBSOCKET_ENDPOINT, token);
        }
    }

    @Override
    public void changeRoom(String room) {
        webSocketClient.unsubscribeAll();
        sendToRoom(NOTIFY_URL_FOR_ROOM_CHANGED);
        setCurrentRoom(room);
        webSocketClient.subscribe(SUBSCRIBE_URL_UPDATE_MARK + SLASH + room, this::onMarkerUpdate);
        webSocketClient.subscribe(SUBSCRIBE_URL_SYNCRONIZE + SLASH + room, this::onSynchronize);
        webSocketClient.subscribe(SUBSCRIBE_URL_FOR_USER_CHANGING_ROOM + SLASH + room, this::onUserChangedRoom);
    }

    @Override
    public void refreshRooms() {
        webSocketClient.subscribe(SUBSCRIBE_URL_REFRESH_ROOMS, this::onFetchRooms);
        webSocketClient.sendEchoViaStomp(REQUEST_URL_REFRESH_ROOMS);
    }

    @Override
    public void refreshUserMarker(String marker) {
        sendToRoom(REQUEST_URL_UPDATE_MARKER, marker);
    }

    @Override
    public void synchronizeMarkers() {
        sendToRoom(REQUEST_URL_SYNCRONZE_MARKER);
    }

    @Override
    public void disconnect() {
        sendToRoom(NOTIFY_URL_FOR_ROOM_CHANGED);
        webSocketClient.disconnectStomp();
    }

    private void setCurrentRoom(String room) {
        this.currentRoom = room;
    }

    private void sendToRoom(String url, String data) {
        if(currentRoom!=null){
            webSocketClient.sendEchoViaStomp(url + SLASH + currentRoom, data);
        }
    }

    private void sendToRoom(String url) {
        if(currentRoom!=null){
            webSocketClient.sendEchoViaStomp(url + SLASH + currentRoom);
        }
    }

    private void onFetchRooms(String rooms) {
        Log.i(TAG, "Received following rooms:" + rooms);
        List<String> listOfRooms = mGson.fromJson(rooms, this.listOfRooms);
        roomsListener.getRooms(listOfRooms);
    }

    private void onMarkerUpdate(String marker) {
        MarkerResponse markerWrapperReceived = mGson.fromJson(marker, MarkerResponse.class);
        markerListener.onMarkerUpdate(markerWrapperReceived);
    }

    private void onSynchronize(String listOfMarkers) {
        Log.i(TAG, "onSyncro with list: " + listOfMarkers);
        List<MarkerResponse> listReceived = mGson.fromJson(listOfMarkers, listOfMarkerResponseType);
        markerListener.onGetAllMarkers(listReceived);
    }

    private void onUserChangedRoom(String username){
        Log.i(TAG, "user with username changed room: " + username);
        Log.i(TAG, username.length() + username.trim());
        markerListener.onMarkerRemoved(username);
    }
}
