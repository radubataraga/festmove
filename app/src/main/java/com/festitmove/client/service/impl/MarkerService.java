package com.festitmove.client.service.impl;

import android.util.Log;

import com.festitmove.client.model.MarkerResponse;
import com.festitmove.client.model.User;
import com.festitmove.client.repository.MarkerRepository;
import com.festitmove.client.repository.UserRepository;
import com.festitmove.client.service.CurrentUserMarkerHandler;
import com.festitmove.client.service.listener.MarkerListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static com.festitmove.client.activity.util.PermissionUtils.logThread;

public class MarkerService implements MarkerListener, CurrentUserMarkerHandler {

    private static final String TAG = MarkerService.class.getSimpleName();
    private User currentUser;
    private final MarkerRepository markerRepository;
    private final UserRepository userRepository;
    private ReentrantLock synchronizationLock = new ReentrantLock();

    public MarkerService(String currentUsername, MarkerRepository markerRepository, UserRepository userRepository) {
        this.markerRepository = markerRepository;
        this.userRepository = userRepository;
        this.currentUser = userRepository.getUserByName(currentUsername);
    }

    @Override
    public void onGetAllMarkers(List<MarkerResponse> markerList) {

        // This will be the only time the synchronization lock gets blocked.
        try {
            if (synchronizationLock.tryLock(6, TimeUnit.SECONDS)) {
                Log.i(TAG, "apel la markerRepository syncro in the currentThread: " + Thread.currentThread().getName() + markerList);
                markerRepository.synchronize(markerList);
                synchronizationLock.unlock();
            }
        } catch (InterruptedException ignored) {
        }
    }

    @Override
    public void onMarkerUpdate(MarkerResponse markerResponse) {

        logThread(TAG);
            /*
                If the synchronization it's going on, then we will abort the update.
                If the response is the request sent by the current user, then abort the update.
             */
        if (!synchronizationLock.isLocked() && !markerResponse.getName().equals(currentUser.getUsername())) {
            Log.i(TAG, "onMarkerUpdate in Service with the currentUsername: " + currentUser.getUsername() + " and resp: " +
                    markerResponse);
            User user = userRepository.getUserByName(markerResponse.getName());
               /*
                This will handle the case when 2 requests come from one android client, and the refference for
                marker is not created
                */
            synchronized (user) {
                Marker marker = markerRepository.getMarkerByUser(user.getUsername());
                if (marker == null) {
                    markerRepository.addMarker(markerResponse);
                } else {
                    markerRepository.updateMarker(markerResponse);
                }
            }
        }
    }

    @Override
    public void onMarkerRemoved(String username) {

        logThread(TAG);
             /*
                If the synchronization it's going on, then we will abort the update.
                If the response is the request sent by the current user, then abort the update.
             */
        if (!synchronizationLock.isLocked() && !username.equals(currentUser.getUsername())) {
            markerRepository.removeMarkerByUsername(username);
        }
    }

    @Override
    public void refreshUserMarker(LatLng latLng) {
        if (currentUser != null && latLng != null)
            markerRepository.refreshUserMarker(currentUser.getUsername(), latLng);
    }
}
