package com.festitmove.client.service.listener;

import com.festitmove.client.model.MarkerResponse;

import java.util.List;

public interface MarkerListener {

    /**
     *  This method notifies when the server sends the list of markers
     *  for synchronization.
     * @param message list of MarkerResponse received from the server
     */
    void onGetAllMarkers(List<MarkerResponse> message);

    /**
     *  This method notifies when the servers sends one user marker for update
     * @param message the {@link MarkerResponse} received from the server
     */
    void onMarkerUpdate(MarkerResponse message);

    /**
     * This method notifies when a user leaves the room.
     * @param username the user's username.
     */
    void onMarkerRemoved(String username);
}
