package com.festitmove.client.service;

import com.google.android.gms.maps.model.LatLng;

public interface CurrentUserMarkerHandler {

    /**
     *  This is used for updating current user position.
     *
     * @param latLng currentUser gps coordinates
     */
    void refreshUserMarker(LatLng latLng);
}
