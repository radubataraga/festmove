package com.festitmove.client.repository.impl;

import com.festitmove.client.model.User;
import com.festitmove.client.repository.UserRepository;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class UserRepositoryImpl implements UserRepository {

    private ConcurrentMap<String, User> userReference;

    public UserRepositoryImpl() {
        this.userReference = new ConcurrentHashMap<>();
    }

    @Override
    public User getUserByName(String username) {
        userReference.putIfAbsent(username, new User(username));
        return userReference.get(username);
    }
}
