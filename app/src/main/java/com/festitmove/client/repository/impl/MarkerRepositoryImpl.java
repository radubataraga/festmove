package com.festitmove.client.repository.impl;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.festitmove.client.model.MarkerResponse;
import com.festitmove.client.repository.MarkerRepository;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class MarkerRepositoryImpl implements MarkerRepository {

    private static final String TAG = MarkerRepositoryImpl.class.getSimpleName();
    private final GoogleMap googleMap;
    private ConcurrentMap<String, Marker> markersReference;

    public MarkerRepositoryImpl(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.markersReference = new ConcurrentHashMap<>();
    }

    /**
     * Executed in UI thread.
     *
     * @param markerResponseList the list of markers to be updated
     */
    @Override
    public void synchronize(List<MarkerResponse> markerResponseList) {
        new Handler(Looper.getMainLooper())
                .post(() -> {
                    Log.i(TAG, "on markerRepo doing syncronization");
                    googleMap.clear();
                    markersReference = new ConcurrentHashMap<>();
                    for (MarkerResponse markerResponse : markerResponseList) {
                        LatLng latLng = new LatLng(markerResponse.getLatitude(), markerResponse.getLongitude());
                        Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(markerResponse.getName()));
                        markersReference.put(markerResponse.getName(), marker);
                    }
                });
    }

    @Override
    public void addMarker(MarkerResponse markerResponse) {
        new Handler(Looper.getMainLooper())
                .post(() -> {
                    Log.i(TAG, "on markerRepo creating marker");
                    LatLng latLng = new LatLng(markerResponse.getLatitude(), markerResponse.getLongitude());
                    Marker marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(markerResponse.getName()));
                    markersReference.put(markerResponse.getName(), marker);
                });
    }

    @Override
    public void updateMarker(MarkerResponse markerResponse) {
        new Handler(Looper.getMainLooper())
                .post(() -> {
                    Log.i(TAG, "on markerRepo updateing marker");
                    Marker marker = markersReference.get(markerResponse.getName());
                    marker.setPosition(new LatLng(markerResponse.getLatitude(), markerResponse.getLongitude()));
                });
    }

    @Override
    public void refreshUserMarker(String user, LatLng latLng) {
        new Handler(Looper.getMainLooper())
                .post(() -> {
                    Log.i(TAG, "on markerRepo refreshing user marker");
                    Marker marker = markersReference.get(user);
                    if (marker == null) {
                        marker = googleMap.addMarker(new MarkerOptions().position(latLng).title(user));
                    } else {
                        marker.setPosition(latLng);
                    }
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    //zoomTo mai mare > mai aproape 6 8 10 indicat pentru regiuni
                    // zoomTo mai mic < mai departat
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(6));
                    markersReference.put(user, marker);
                });
    }

    @Override
    public void removeMarkerByUsername(String username) {
        new Handler(Looper.getMainLooper())
                .post(() -> {
                    Log.i(TAG, "on markerRepo removing marker from user " + username);
                    Marker marker = markersReference.get(username);
                    if (marker != null) {
                        Log.i(TAG, "marker not null, now removing " + marker);
                        marker.remove();
                        markersReference.remove(username);
                    }
                });
    }

    /*
       It's not thread-safe, the syncronization must be made outside the class.
     */
    @Override
    public Marker getMarkerByUser(String name) {
        return markersReference.get(name);
    }
}
