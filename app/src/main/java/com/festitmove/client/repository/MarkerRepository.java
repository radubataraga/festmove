package com.festitmove.client.repository;

import com.festitmove.client.model.MarkerResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

/**
 * All methods are thread-safe expect {@link #getMarkerByUser}.
 */
public interface MarkerRepository {

    /**
     * Cleans the google map and re-populates it with the given markerResponseList
     * @param markerResponseList the markers list
     */
    void synchronize(List<MarkerResponse> markerResponseList);

    /**
     * Adds a new marker on the map
      * @param markerResponse the new marker's position
     */
    void addMarker(MarkerResponse markerResponse);

    /**
     * Updates a marker with the {@param markerResponse} name and
     * with it's position.
     * @param markerResponse the given markerResponse
     */
    void updateMarker(MarkerResponse markerResponse);

    /**
     *  Refreshes the location and camera position for the currently connected user.
     * @param user the currently connected user.
     * @param latLng the locations coordonates that the marker should use
     */
    void refreshUserMarker(String user, LatLng latLng);


    /**
     *  It's not thread-safe, the synchronization must be made outside the class.
     *  This gets a marker by username
     * @param name the given username
     * @return the marker
     */
    Marker getMarkerByUser(String name);

    /**
     * Removes a marker from the map.
     * @param username the name of user that marker should be removed.
     */
    void removeMarkerByUsername(String username);
}
