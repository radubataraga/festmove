package com.festitmove.client.repository;

import com.festitmove.client.model.User;

public interface UserRepository {
    /**
     * If the given username is not in the repository, then
     * a new one will be created.
     * @param username the given username
     * @return the user with the given name. Otherwise, retuns a new created user
     */
    User getUserByName(String username);
}
